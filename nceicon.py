from tkinter import *
from tkinter import Button as Button_old
import tkinter.filedialog
from tkinter.ttk import *
from tkinter.scrolledtext import ScrolledText
from multiprocessing import Pool, freeze_support, Process, Manager
import numpy as np
import cv2
import zlib
from PIL import Image, ImageTk, ImageFont, ImageDraw, ImageFilter
from time import time, sleep
import os, sys
import yaml
import io
from random import choice
from glob import glob
from canvasimage import Zoom_Advanced

def get_handle(text):
    if not isinstance(text, str):
        text = " ".join(text)
    if text[0].lower()!=text[0].upper():
        delimit = " "
    else:
        delimit = text[0]
        text = text[1:]
    d = {"tags": []}
    for tag in text.split(delimit):
        if ":" in tag:
            key, value = tag.split(":",1)
            d[key] = value
        else:
            d["tags"].append(tag)
    d["tags"] = " ".join(d["tags"])

    if "name" in list(d.keys()) and "author" in list(d.keys()):
        base = f"{d['name']} by {d['author']}"
    elif "name" in list(d.keys()):
        base = f"{d['name']}"
    else:
        base = "CEI file"
    additional = ", tags: "+d["tags"] if d["tags"] else ""
    return base+additional

def read_for_pil(fname):
    with open(fname, "rb") as fp:
        im_b = fp.read()
    bio = io.BytesIO(im_b)
    bio.seek(0)
    return Image.open(bio)

def read_for_cv2(fname):
    with open(fname, "rb") as fp:
        im_b = fp.read()
    image_np = np.frombuffer(im_b, np.uint8)
    img_np = cv2.imdecode(image_np, 0)
    return img_np

def get_cwd():
    if getattr(sys, 'frozen', False):
        application_path = os.path.dirname(sys.executable)
    elif __file__:
        application_path = os.path.dirname(__file__)
    return application_path

def avg(pixel1,pixel2):
    result = [round((pixel1[n]+pixel2[n])/2) for n in range(len(pixel1))]
    return tuple(result)

def process(path):
    original = Image.open(path).convert("RGB") if isinstance(path,str) else path.convert("RGB")
    modified = original.copy()
    omod = modified.load()
    eight = 16
    for y in range(modified.size[1]):
        for x in range(eight,modified.size[0],eight):
            average = avg(omod[x-1,y],omod[x,y])
            omod[x-1,y],omod[x,y] = avg(omod[x-1,y],average),avg(omod[x,y],average)
    for y in range(eight,modified.size[1],eight):
        for x in range(modified.size[0]):
            average = avg(omod[x,y-1],omod[x,y])
            omod[x,y-1],omod[x,y] = avg(omod[x,y-1],average),avg(omod[x,y],average)
    return modified

def to_ibyte(osize, size):
    c_x = 1 if osize[0]!=16 else 0
    c_y = 1 if osize[1]!=16 else 0
    x = bin(size[0]-1).split("b")[-1]
    y = bin(size[1]-1).split("b")[-1]
    while len(x)<3:
        x = "0"+x
    while len(y)<3:
        y = "0"+y
    return int(f"{x}{c_x}{y}{c_y}", base = 2)

def from_ibyte(ibyte, size):
    ib = bin(ibyte).split("b")[-1]
    while len(ib)<8:
        ib = "0"+ib
    volume = (int(ib[:3], base = 2)+1)*(int(ib[-4:-1], base = 2)+1)
    x = 16 if not int(ib[3]) else size[0]%16
    y = 16 if not int(ib[-1]) else size[1]%16
    return (x,y), ((int(ib[:3], base = 2)+1),(int(ib[-4:-1], base = 2)+1)), volume

def pack(value):
    if type(value)==str:
        v = bytes(value, "utf-8")
    elif type(value)==int:
        v = value.to_bytes(3,"big")
    elif type(value)==bytes:
        v = value
    elif type(value) in [list,tuple]:
        v = separator([pack(valuelet) for valuelet in value])
    return v

def bytedict(dictionary):
    l = []
    for key in list(dictionary.keys()):
        l.append(bytes(key, "utf-8"))
        value = dictionary[key]
        l.append(pack(value))

    return separator(l)

def cei_dict(bd):
    l = sepget(bd)
    d = dict()
    for n in range(0, len(l), 2):
        match str(l[n], "utf-8"):
            case "version":
                value = int.from_bytes(l[n+1], "big")
            case "size" | "csize":
                value = tuple([int.from_bytes(i, "big") for i in sepget(l[n+1])])
            case "luminocity" | "chromaticity" | "palette":
                value = zlib.decompress(l[n+1])
            case "tags":
                value = str(l[n+1], "utf-8")#tuple([str(i, "utf-8") for i in sepget(l[n+1])])
            
        d[str(l[n],"utf-8")] = value
    return d
        
def separator(byteseq):
    sequence = b""
    for obj in byteseq:
        first = len(obj).to_bytes(8, "big")
        while first[0]==0:
            first = first[1:]
        second = bytes([len(first)])
        sequence+=obj+first+second
    return sequence

def sepget(sequence):
    items = []
    while len(sequence):
        prelen = sequence[-1]
        sequence = sequence[:-1]
        length = int.from_bytes(sequence[-prelen:], "big")
        sequence = sequence[:-len(sequence[-prelen:])]
        items = [sequence[-length:]] + items
        sequence = sequence[:-length]
        
    return items

def get_color(fname, size):
    image = Image.open(fname).resize(size, Image.Resampling.LANCZOS).convert("P", dither = Image.FLOYDSTEINBERG, palette = Image.ADAPTIVE).convert("RGB")
    data = tuple(image.getdata())
    color_index = 0
    colors = dict()
    colors_list = []
    for tup in data:
        if not tup in colors_list:
            colors_list.append(tup)
            colors[color_index] = tup
            color_index+=1
        if color_index>=256:
            break
    lss_colors = dict()
    for key in list(colors.keys()):
        lss_colors[colors[key]] = to_LSS(colors[key])
    color_data = []
    color_data_bytes = b""
    for pixel in data:
        tup = lss_colors[pixel][1:]
        color_data.append(tup)
        color_data_bytes+=bytes(tup)
    return color_data_bytes

def get_color_palette(fname, size):
    image = Image.open(fname).resize(size, Image.Resampling.LANCZOS).convert("RGB")
    data = tuple(image.getdata())
    with Pool(8) as p:
        lss_data = p.map(to_LSS, data)
    luma_clear = [(0, i[1], i[2]) for i in lss_data]
    color_image = Image.new("RGB", image.size)
    color_image.putdata(tuple(luma_clear))
    color_image = color_image.convert("P", dither = Image.FLOYDSTEINBERG, palette = Image.ADAPTIVE).convert("RGB")
    data = tuple(color_image.getdata())
    palette = []
    for pixel in data:
        if not pixel[1:] in palette:
            palette.append(pixel[1:])
        if len(palette)==256:
            break
    palette_bytes = b""
    for p in palette:
        palette_bytes+=bytes(p)
    pixel_bytes = bytes([palette.index(i[1:]) for i in data])
    return palette_bytes, pixel_bytes  
        
def to_LSS(RGB):#convert rgb to luminescence, ratio_1, ratio_2 (ratio is sootnosheniye in rus)
    R,G,B = RGB[:3]
    L = (R+G+B) / 3
    try:
        S_1 = R*255/(R+G)
    except ZeroDivisionError:
        S_1 = 128
    try:
        S_2 = G*255/(G+B)
    except ZeroDivisionError:
        S_2 = 128
    L,S_1,S_2 = round(L),round(S_1),round(S_2)
    return (L,S_1,S_2)

def from_LSS(LSS):#convert lss back to rgb
    L,S_1,S_2 = LSS
    try:
        s1 = S_1/(255-S_1)
    except ZeroDivisionError:
        s1 = S_1
    try:
        s2 = (255-S_2)/S_2
    except ZeroDivisionError:
        s2 = 255
    G = 3*L / ( s1 + s2 + 1 )#source[n*3]
    try:
        R = (S_1*G)/(255-S_1)
    except ZeroDivisionError:
        R = (S_1*G)/1
    try:
        B = G*(255-S_2)/S_2
    except ZeroDivisionError:
        B = G*(255-S_2)/1
    maximal = max([R,G,B])
    if maximal > 255:
        multiplier = 255/maximal
        R,G,B = R*multiplier,G*multiplier,B*multiplier
    R,G,B = round(R),round(G),round(B)
    return (R,G,B)

def np_lss(L,S_1,S_2):
    try:
        s1 = S_1/(255-S_1)
    except ZeroDivisionError:
        s1 = S_1
    try:
        s2 = (255-S_2)/S_2
    except ZeroDivisionError:
        s2 = 255
    G = 3*L / ( s1 + s2 + 1 )#source[n*3]
    try:
        R = (S_1*G)/(255-S_1)
    except ZeroDivisionError:
        R = (S_1*G)/1
    try:
        B = G*(255-S_2)/S_2
    except ZeroDivisionError:
        B = G*(255-S_2)/1
    maximal = max([R,G,B])
    if maximal > 255:
        multiplier = 255/maximal
        R,G,B = R*multiplier,G*multiplier,B*multiplier
    R,G,B = round(R),round(G),round(B)
    return R,G,B

def np_LSS(LSS):#convert lss back to rgb
    L, S_1, S_2 = cv2.split(LSS.astype(np.float32))
    vnp_lss = np.vectorize(np_lss)
    merged = cv2.merge(vnp_lss(L,S_1,S_2))
    #print(merged[0])
    #cv2.imshow("sus", merged.astype(np.uint8))
    return Image.fromarray(merged.astype(np.uint8))

def np_LSS_procedural(LSS, obj):
    L, S_1, S_2 = cv2.split(LSS.astype(np.float32))
    vnp_lss = np.vectorize(np_lss)
    parts = 16

    obj[:] = [None for i in range(parts)]

    length = LSS.shape[0]
    L_list = [L[int(length*i/parts):int(length*(i+1)/parts),:] for i in range(parts)]
    S_1_list = [S_1[int(length*i/parts):int(length*(i+1)/parts),:] for i in range(parts)]
    S_2_list = [S_2[int(length*i/parts):int(length*(i+1)/parts),:] for i in range(parts)]

    for n in range(parts):
        l, s1, s2 = [i[n] for i in [L_list,S_1_list,S_2_list]]
        print((l,s1,s2))
        merged = cv2.merge(vnp_lss(l,s1,s2))
        obj[n] = Image.fromarray(merged.astype(np.uint8))

def sort_sectors(sects):
    sects = sorted(sects)
    new_sects = [sum(sects[:n+1]) for n in range(len(sects))]
    return new_sects

def sector_process(tup):
    return tup[0].render(*tup[1])

def get_luma(fname, c):
    print(f"Getting luma from {fname}")
    bw = read_for_cv2(fname)#cv2.imread(fname,0)
    h, w = bw.shape
    total_bytes = b""
    objects = []
    for y in range(0, h, 16):
        for x in range(0, w, 16):
            wid = min([16, w-x])
            hig = min([16, h-y])
            array = bw[y:y+hig,x:x+wid]
            obj = LumaSector(array)
            objects.append(obj)
    maximal_x = max([obj.get_dev()[1] for obj in objects])
    maximal_y = max([obj.get_dev()[0] for obj in objects])
    minimal_x = min([obj.get_dev()[1] for obj in objects])
    minimal_y = min([obj.get_dev()[0] for obj in objects])
    dxy = (sort_sectors([obj.get_dev()[1] for obj in objects]), sort_sectors([obj.get_dev()[0] for obj in objects]))
    pool_args = []
    for n, obj in enumerate(objects):
        #print(f"{round(100*n/len(objects),3)}%")
        pool_args.append((obj,(c, (maximal_x, maximal_y), (minimal_x, minimal_y), dxy)))
        #b_obj = obj.render(c, (maximal_x, maximal_y), (minimal_x, minimal_y), dxy)
        #total_bytes += b_obj
    with Pool(8) as p:
        iterator = p.imap(sector_process, pool_args)
        n = 0
        for bobject in iterator:
            if n%10==0:
                print(f"{round(100*n/len(objects),3)}%")
            total_bytes+=bobject
            n+=1
    return total_bytes

def create_cei(fname, formula, chroma_size, tags):
    luma = get_luma(fname, formula)
    palette, chroma = get_color_palette(fname, chroma_size)
    d = {"version": 0,
         "size": Image.open(fname).size,
         "luminocity": zlib.compress(luma, 9),
         "palette": zlib.compress(palette, 9),
         "chromaticity": zlib.compress(chroma, 9),
         "csize": chroma_size,
         "tags": tags}

    b = bytedict(d)
    #with open(f"{fname.rsplit('.',1)[0]}.cei", "wb") as f:
    #    f.write(b)
    return b

def open_cei_preview(fname):
    return fname, *open_cei(fname, preview = True)

def open_cei(fname, preview = False):
    with open(fname, "rb") as f:
        d = cei_dict(f.read())
    print(list(d.keys()))
    luma = d["luminocity"]
    focus = 0
    sectors = []
    luma_bg = np.ndarray((d["size"][1],d["size"][0]), dtype = np.uint8)
    while focus<len(luma):
        sec_size, red_size, volume = from_ibyte(luma[focus], d["size"])
        bytes_ = luma[focus+1:focus+volume+1]
        #print(red_size)
        array = np.array([int(i) for i in bytes_], dtype = np.uint8).reshape((red_size[1], red_size[0]))
        sectors.append(cv2.resize(array, sec_size, interpolation = cv2.INTER_LINEAR))
        focus+=volume+1
    for y in range(0, d["size"][1], 16):
        for x in range(0, d["size"][0], 16):
            sector = sectors.pop(0)
            h, w = sector.shape
            luma_bg[y:y+h,x:x+w] = sector
    #cv2.imshow("img",luma_bg)
    if preview:
        return Image.fromarray(luma_bg), d["tags"]
    pal_ind = 0
    palette = dict()
    for n in range(0, len(d["palette"]), 2):
        palette[pal_ind] = tuple(d["palette"][n:n+2])
        pal_ind+=1

    colors = np.array([0,palette[0][0],palette[0][1]], dtype = np.uint8)#[]
    print(colors)
    #chroma_bg = Image.new("RGB", d["csize"])
    csize = d["csize"]
    for n, index in enumerate(d["chromaticity"]):
        if n:
            colors = np.append(colors, np.array([0,palette[index][0],palette[index][1]], dtype = np.uint8), axis = 0)

    #chroma_bg.putdata(tuple(colors))
    #chroma_bg.show()
    print(colors)
    lss = colors.reshape(csize[1],csize[0],3)
    lss = cv2.resize(lss, d["size"], interpolation = cv2.INTER_LINEAR)
    lss[:,:,0] = luma_bg
    return np_LSS(lss), d["tags"]

    lss_list = []
    for y in range(d["size"][1]):
        for x in range(d["size"][0]):
            lss_list.append(tuple(lss[y,x]))

    with Pool(8) as p:
        rgb_tuple = tuple(p.map(from_LSS, tuple(lss_list)))
    image_bg = Image.new("RGB", d["size"])
    image_bg.putdata(rgb_tuple)
    return image_bg

def icon():
    return os.path.join(get_cwd(),"plogo32.ico")

class LumaSector:
    def __init__(self, array):
        self.array = array

    def get_dev(self):
        hdev, wdev = np.std(self.array, axis = 0), np.std(self.array, axis = 1)
        #print(wdev.sum(),hdev.sum())
        return float(hdev.sum()), float(wdev.sum())

    def render(self, c, maximals, minimals, distribution = None):
        mx, my = maximals
        mix, miy = minimals
        hd, wd = self.get_dev()
        ohig, owid = self.array.shape
        
        xx = (wd-mix)/(mx-mix)
        yy = (hd-miy)/(my-miy)
        
        if distribution:
            dx, dy = distribution
            xsum, ysum = dx[-1], dy[-1]
            focus = 0
            while focus<len(dx)and dx[focus]/xsum<xx:
                focus+=1
            xx = focus/len(dx)
            focus = 0
            while focus<len(dy) and dy[focus]/ysum<yy:
                focus+=1
            yy = focus/len(dy)
        if type(c)==str:
            new_width = round(owid*eval(c, {"x": xx}))
            new_height = round(ohig*eval(c, {"x": yy}))
        else:
            new_width = round(owid*c[int((len(c)-1)*xx)])
            new_height = round(ohig*c[int((len(c)-1)*yy)])
        if new_height<1:
            new_height+=1
        elif new_height>8:
            new_height = 8
        if new_width<1:
            new_width+=1
        elif new_width>8:
            new_width = 8
        resized = cv2.resize(self.array, (new_width, new_height), interpolation = cv2.INTER_AREA)
        content = bytes(resized.flatten())
        index = to_ibyte((owid,ohig), (new_width,new_height))
        """
        index = (resized.shape[-1]-1)*16 + resized.shape[0]-1
        if owid!=16:
            index+=16
        if ohig!=16:
            index+=1
        """
        size = bytes([index])
        return size+content

    def get_bytes(self):
        return bytes(self.array.flatten())

class GUI(Tk):
    def __init__(self):
        super().__init__()
        self.title("Numpy-enabled CEICon")
        self.iconbitmap(icon())
        self.post_processing = True
        self.search_shown = False
        self.directory = ""

        style = Style()
        style.configure("Popup.TEntry", padding = [0,0,0,0])
        style.configure("Popup.TLabel", padding = [0,0,10,0])
        style.configure("Custom.TLabel", padding = [10,0,10,0])

        menu_dict = {"borderwidth": 0, "tearoff":0}
        self.menubar = Menu(self, **menu_dict)
        self.file_tab = Menu(self.menubar, **menu_dict)
        self.file_tab.add_command(label = "Convert to CEI", command = self.convert)
        self.file_tab.add_command(label = "Sliders to CEI", command = self.convert_sliders)
        self.file_tab.add_command(label = "Open CEI", command = self.open_procedural)
        self.file_tab.add_command(label = "Open CEI Preview", command = self.open_preview)
        self.file_tab.add_command(label = "Preview Directory", command = self.open_dir)
        self.menubar.add_cascade(label = "File", menu = self.file_tab)#
        self.settings_tab = Menu(self.menubar, **menu_dict)
        self.settings_tab.add_command(label = "Configure", command = self.change_settings)
        #self.settings_tab.add_command(label = "Toggle Theme", command = lambda: sv_ttk.toggle_theme())
        self.menubar.add_cascade(label = "Settings", menu = self.settings_tab)#
        self.config(menu = self.menubar)

        self.search_var = StringVar()
        self.search = Entry(self, textvariable = self.search_var, font = ("Calibri", 22))

        self.text = WaterText(self, wrap = CHAR, relief = FLAT)
        self.text.pack(fill = BOTH, expand = True)
        self.bind("<Control-f>", self.toggle_search)
        self.search.bind("<Return>", self.open_dir_keyword)
        
        self.mainloop()

    def toggle_search(self, event = None):
        self.text.forget()
        self.search.forget()
        self.search_shown = not self.search_shown
        if self.search_shown:
            self.search.pack(fill = X, side = TOP)
        self.text.pack(fill = BOTH, expand = True)

    def open_dir(self):
        directory = tkinter.filedialog.askdirectory(parent = self, title = "Select a CEI directory")
        self.directory = directory
        self.text.load_dir(directory)

    def open_dir_keyword(self, event):
        if not self.directory:
            self.directory = tkinter.filedialog.askdirectory(parent = self, title = "Select a CEI directory")
        print(self.search_var.get())
        self.text.load_dir(self.directory, tags = self.search_var.get())

    def change_settings(self):
        inp = Input(self,[("<cache>","cei_settings"),
                          ("<ok>","Save"),
                          ("<choice>","Process:",["Remove grid","Do not modify"])])
        self.post_processing = {"Remove grid": True, "Do not modify": False}[inp.input[0]]

    def convert(self):
        inp = Input(self,[("<cache>","settings"),
                          ("<out>","Output:"),
                          ("<path>","Source:"),
                          ("<choice>","Formula:",["x","x**0.5","x**0.25","x**2"]),
                          ("<slider>","Chroma:",0,100),
                          "Tags:"])
        chroma_size = tuple([round(i*inp.input[3]/100) for i in read_for_pil(inp.input[1]).size])
        b = create_cei(inp.input[1], inp.input[2], chroma_size, inp.input[-1])
        with open(inp.input[0],"wb") as f:
            f.write(b)

    def convert_sliders(self):
        inp = Input(self,[("<cache>","settings_slders"),
                          ("<out>","Output:"),
                          ("<path>","Source:"),
                          ("<slider>","Level 1:",0,100),
                          ("<slider>","Level 2:",0,100),
                          ("<slider>","Level 3:",0,100),
                          ("<slider>","Level 4:",0,100),
                          ("<slider>","Level 5:",0,100),
                          ("<slider>","Level 6:",0,100),
                          ("<slider>","Level 7:",0,100),
                          ("<slider>","Level 8:",0,100),
                          ("<slider>","Level 9:",0,100),
                          ("<slider>","Level 10:",0,100),
                          ("<slider>","Chroma:",0,100),
                          "Tags:"])
        chroma_size = tuple([round(i*inp.input[-2]/100) for i in read_for_pil(inp.input[1]).size])
        b = create_cei(inp.input[1], [i/100 for i in inp.input[2:12]], chroma_size, inp.input[-1])
        with open(inp.input[0],"wb") as f:
            f.write(b)

    def open(self):
        fname = tkinter.filedialog.askopenfilename(title = "Select CEI to view",
                                                   filetypes = [("Color-Encoded Image","*.cei")])
        im, text = open_cei(fname)
        handle = self.get_handle(text)
        if self.post_processing:
            im = process(im)
            Viewer(self, im, handle)
        else:
            Viewer(self, im, handle)

    def open_procedural(self):
        fname = tkinter.filedialog.askopenfilename(title = "Select CEI to view",
                                                   filetypes = [("Color-Encoded Image","*.cei")])
        Viewer(self, None, "Loading CEI file...", fname)

    def open_preview(self):
        fname = tkinter.filedialog.askopenfilename(title = "Select CEI to view",
                                                   filetypes = [("Color-Encoded Image","*.cei")])
        im, text = open_cei(fname, preview = True)
        handle = self.get_handle(text)
        if self.post_processing:
            im = process(im)
            Viewer(self, im, handle)
        else:
            Viewer(self, im, handle)

    def get_handle(self, text):
        if not isinstance(text, str):
            text = " ".join(text)
        if text[0].lower()!=text[0].upper():
            delimit = " "
        else:
            delimit = text[0]
            text = text[1:]
        d = {"tags": []}
        for tag in text.split(delimit):
            if ":" in tag:
                key, value = tag.split(":",1)
                d[key] = value
            else:
                d["tags"].append(tag)
        d["tags"] = " ".join(d["tags"])

        if "name" in list(d.keys()) and "author" in list(d.keys()):
            base = f"{d['name']} by {d['author']}"
        elif "name" in list(d.keys()):
            base = f"{d['name']}"
        else:
            base = "CEI file"
        additional = ", tags: "+d["tags"] if d["tags"] else ""
        return base+additional

class Input(Toplevel):
    """The input class, collects labels as a list of tuples/strings,
strings default to Entry widgets"""
    def __init__(self, master, labels, **kwargs):
        super().__init__(master, **kwargs)
        self.resizable(True,False)
        self.iconbitmap(icon())
        #self.master = master.root
        self.label_widgets = []
        self.interactive_widgets = []
        self.interactive_hooks = []
        self.rows = []
        self.commands = []
        self.cache = None
        self.cache_list = []
        self.ok = "Generate"#Ok button text
        wid = 8 # label width, in units
        for item in labels:
            if type(item)==str:
                self.rows.append( Frame(self, padding = (20,0,20,0)) )
                self.label_widgets.append( Label(self.rows[-1], text = item, width = wid, style = "Popup.TLabel") )
                self.interactive_hooks.append( StringVar() )
                self.interactive_widgets.append( Entry(self.rows[-1], textvariable = self.interactive_hooks[-1], style = "Popup.TEntry") )

                self.label_widgets[-1].pack(side = LEFT)
                self.interactive_widgets[-1].pack(side = LEFT, fill = X, expand = True)
                self.rows[-1].pack(fill = X)
            else:
                if item[0] == "<entry>":
                    pass
                elif item[0] == "<ok>":
                    self.ok = item[1]
                elif item[0] == "<cache>":
                    self.cache = item[1]+".cache"
                    if os.path.exists(self.cache):
                        with open(self.cache) as f:
                            self.cache_list = yaml.safe_load(f.read())
                elif item[0] == "<choice>":
                    self.rows.append( Frame(self, padding = (20,0,20,0)) )
                    self.label_widgets.append( Label(self.rows[-1], text = item[1], width = wid, style = "Popup.TLabel") )
                    self.interactive_hooks.append( StringVar() )
                    self.interactive_hooks[-1].set(item[2][0])
                    self.interactive_widgets.append( Combobox(self.rows[-1], textvariable = self.interactive_hooks[-1], values = item[2]) )

                    self.label_widgets[-1].pack(side = LEFT)
                    self.interactive_widgets[-1].pack(side = LEFT, fill = X, expand = True)
                    self.rows[-1].pack(fill = X)
                elif item[0] == "<slider>":
                    self.rows.append( Frame(self, padding = (20,0,20,0)) )
                    self.label_widgets.append( Label(self.rows[-1], text = item[1], width = wid, style = "Popup.TLabel") )
                    self.interactive_hooks.append( IntVar() )
                    self.interactive_widgets.append( CustomScale(self.rows[-1],
                                                           variable = self.interactive_hooks[-1],
                                                           from_ = item[2],
                                                           to = item[3]) )

                    self.label_widgets[-1].pack(side = LEFT)
                    self.interactive_widgets[-1].pack(side = LEFT, fill = X, expand = True)
                    self.rows[-1].pack(fill = X)
                elif item[0] == "<path>":
                    self.rows.append( Frame(self, padding = (20,0,20,0)) )
                    self.label_widgets.append( Label(self.rows[-1], text = item[1], width = wid, style = "Popup.TLabel") )
                    self.interactive_hooks.append( StringVar() )
                    self.interactive_widgets.append( Path(self.rows[-1],
                                                           self.interactive_hooks[-1]) )

                    self.label_widgets[-1].pack(side = LEFT)
                    self.interactive_widgets[-1].pack(side = LEFT, fill = X, expand = True)
                    self.rows[-1].pack(fill = X)
                elif item[0] == "<out>":
                    self.rows.append( Frame(self, padding = (20,0,20,0)) )
                    self.label_widgets.append( Label(self.rows[-1], text = item[1], width = wid, style = "Popup.TLabel") )
                    self.interactive_hooks.append( StringVar() )
                    self.interactive_widgets.append( Out(self.rows[-1],
                                                           self.interactive_hooks[-1]) )

                    self.label_widgets[-1].pack(side = LEFT)
                    self.interactive_widgets[-1].pack(side = LEFT, fill = X, expand = True)
                    self.rows[-1].pack(fill = X)
            if self.cache_list and self.interactive_hooks and labels[-1][0]!="<cache>":
                self.interactive_hooks[-1].set(self.cache_list.pop(0))
                if item[0] == "<slider>":
                    self.interactive_widgets[-1].right["text"] = str(self.interactive_hooks[-1].get())
            
        self.final_row = Frame(self, padding = (20,0,20,0))
        self.accept_button = Button(self.final_row, text = self.ok, command = self.action)
        self.accept_button.pack(fill = BOTH)
        self.final_row.pack(fill = X)
        self.mainloop()

    def action(self):
        self.input = []
        for item in self.interactive_hooks:
            self.input.append(item.get())
        if self.cache:
            with open(self.cache, "w") as f:
                f.write(yaml.dump(self.input))
        self.destroy()
        self.quit()

    def autocomplete(self, source):
        cei = source.rsplit(".",1)[0]+".cei"
        for widget in self.interactive_widgets:
            if type(widget)==Out:
                widget.hook.set(cei)

class CustomScale(Frame):
    def __init__(self, master, variable, from_, to):
        super().__init__(master)
        self.master = master
        self.var = variable
        #self.var.set(32)
        self.left = Scale(self, variable = self.var, command = self.callback, from_ = from_, to = to)
        self.right = Label(self, width = 3, style = "Custom.TLabel")
        self.left.pack(side = LEFT, fill = X, expand = True)
        self.right.pack(side = RIGHT)
        self.right["text"] = str(self.var.get())

    def callback(self, event):
        self.right["text"] = str(self.var.get())

class Path(Frame):
    def __init__(self, master, hook):
        super().__init__(master)
        self.master = master
        self.hook = hook
        self.ent = Entry(self, textvariable = self.hook, style = "Popup.TEntry")
        self.but = Button(self, text = "?", width = 3, command = self.command)

        self.ent.pack(fill = X, expand = True, side = LEFT)
        self.but.pack(side = RIGHT)

    def command(self):
        self.hook.set( tkinter.filedialog.askopenfilename(title = "Select image",
                                                            filetypes = [("Lossless Graphics","*.png"),("Lossy Graphics","*.jpg"),("Other","*")]) )
        self.master.master.autocomplete(self.hook.get())
        self.master.focus_set()

class Out(Frame):
    def __init__(self, master, hook):
        super().__init__(master)
        self.master = master
        self.hook = hook
        self.ent = Entry(self, textvariable = self.hook, style = "Popup.TEntry")
        self.but = Button(self, text = "?", width = 3, command = self.command)

        self.ent.pack(fill = X, expand = True, side = LEFT)
        self.but.pack(side = RIGHT)

    def command(self):
        self.hook.set( tkinter.filedialog.asksaveasfilename(title = "Insert or select output filename",
                                                            defaultextension = ".cei",
                                                            filetypes = [("Color-Encoded Image","*.cei")]) )
        self.master.focus_set()

class Viewer(object):#custom image viewer, change image size using the scrollwheel, click on the image to open in default img opener
    def __init__(self,master,image,text, procedural = False):
        if master!=None:
            self.t = Toplevel(master)
        else:
            self.t = Tk()
        self.t.withdraw()

        if not procedural:
            self.count = 10
            self.image = image
            self.thumb = image.copy()
            self.thumb.thumbnail((1000,1000))
            self.count = round(10*self.thumb.size[1]/self.image.size[1])
            print(type(self.image))
            self.tkimage = ImageTk.PhotoImage(self.image.resize((self.image.size[0]*self.count//10,self.image.size[1]*self.count//10),Image.BICUBIC))#self.image)
        
        #self.b = Button_old(self.t, image=self.tkimage if not procedural else None, command = self.showimg, relief=FLAT, borderwidth=0)
        self.t.bind("<MouseWheel>", self.mouse_wheel)
        self.t.bind("<Control-s>", self.saveimg)

        self.t.iconbitmap(icon())
        
        if not procedural:
            self.t.title(text)

        if procedural:
            self.image = self.open_cei_sequential(procedural)
            self.thumb = self.image.copy()
            self.thumb.thumbnail((1000,1000))
            self.count = round(10*self.thumb.size[1]/self.image.size[1])
        else:
            self.za = Zoom_Advanced(self.t, image)
            self.za.grid()
            self.t.grid_rowconfigure(0, weight = 1)
            self.t.grid_columnconfigure(0, weight = 1)
            self.t.deiconify()

        if master==None:
            self.t.mainloop()

    def update_image(self, image):
        self.za.image = image.copy()
        self.za.show_image()
            
    def mouse_wheel(self,event):
        return
        if event.num == 5 or event.delta == -120:
            if self.count>1: self.count -= 1
        if event.num == 4 or event.delta == 120:
            if self.count<10: self.count += 1
        self.tkimage = ImageTk.PhotoImage(self.image.resize((self.image.size[0]*self.count//10,self.image.size[1]*self.count//10),Image.BICUBIC))
        self.b.config(image = self.tkimage)
        
    def showimg(self):
        self.image.show()
        
    def saveimg(self,useless):
        chars = "qwertyuiopasdfghjklzxcvbnm0123456789"
        name = ""
        for n in range(8):
            name+=choice(chars)
        name+=".png"
        print(name)
        self.image.save(name)
        
    def up(self, image):
        self.image = image
        self.tkimage = self.tkimage = ImageTk.PhotoImage(self.image.resize((self.image.size[0]*self.count//10,self.image.size[1]*self.count//10),Image.BICUBIC))
        self.b.config(image = self.tkimage)
        self.t.update()

    def open_cei_sequential(self, fname, preview = False):
        with open(fname, "rb") as f:
            d = cei_dict(f.read())
        print(list(d.keys()))
        luma = d["luminocity"]
        focus = 0
        sectors = []
        luma_bg = np.ndarray((d["size"][1],d["size"][0]), dtype = np.uint8)
        while focus<len(luma):
            sec_size, red_size, volume = from_ibyte(luma[focus], d["size"])
            bytes_ = luma[focus+1:focus+volume+1]
            #print(red_size)
            array = np.array([int(i) for i in bytes_], dtype = np.uint8).reshape((red_size[1], red_size[0]))
            sectors.append(cv2.resize(array, sec_size, interpolation = cv2.INTER_LINEAR))
            focus+=volume+1
        for y in range(0, d["size"][1], 16):
            for x in range(0, d["size"][0], 16):
                sector = sectors.pop(0)
                h, w = sector.shape
                luma_bg[y:y+h,x:x+w] = sector
        
        self.luma_im = Image.fromarray(luma_bg).convert("RGB")
        self.za = Zoom_Advanced(self.t, self.luma_im)
        self.za.grid()
        self.t.grid_rowconfigure(0, weight = 1)
        self.t.grid_columnconfigure(0, weight = 1)
        li = self.luma_im.copy()
        li.thumbnail((1000,1000))
        tsize = li.size
        #self.za.imscale = tsize[1]/self.luma_im.size[1]
        self.update_image(self.luma_im)
        self.t.geometry(f"{min([self.luma_im.size[0],1000])}x{min([self.luma_im.size[1],1000])}")
        del li
        self.tkimage = ImageTk.PhotoImage(self.luma_im.resize(tsize, Image.BICUBIC))
        #self.b.config(image = self.tkimage)
        self.update_image(self.luma_im)
        self.t.deiconify()
        self.t.title(get_handle(d["tags"]))
        self.t.update()
        if preview:
            return Image.fromarray(luma_bg), d["tags"]
        pal_ind = 0
        palette = dict()
        for n in range(0, len(d["palette"]), 2):
            palette[pal_ind] = tuple(d["palette"][n:n+2])
            pal_ind+=1

        colors = np.array([0,palette[0][0],palette[0][1]], dtype = np.uint8)#[]
        print(colors)
        
        csize = d["csize"]
        for n, index in enumerate(d["chromaticity"]):
            if n:
                colors = np.append(colors, np.array([0,palette[index][0],palette[index][1]], dtype = np.uint8), axis = 0)

        #chroma_bg.putdata(tuple(colors))
        #chroma_bg.show()
        print(colors)
        lss = colors.reshape(csize[1],csize[0],3)
        lss = cv2.resize(lss, d["size"], interpolation = cv2.INTER_LINEAR)
        lss[:,:,0] = luma_bg
        #return np_LSS(lss), d["tags"]

        man = Manager()
        shared_list = man.list([None for i in range(16)])
        proc = Process(target = np_LSS_procedural, args = (lss, shared_list))
        proc.start()
        counter = 0
        height = 0
        while None in shared_list[:] or counter<16:
            sleep(0.05)
            if 16-shared_list[:].count(None)>counter:
                self.luma_im.paste(shared_list[counter], (0,height))
                self.tkimage = ImageTk.PhotoImage(self.luma_im.resize(tsize, Image.BICUBIC))
                #self.b.config(image = self.tkimage)
                self.update_image(self.luma_im)
                height+=shared_list[counter].size[1]
                counter+=1
                self.t.update()
        return self.luma_im

class WaterText(ScrolledText):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def load_dir(self, directory, tags = None):
        files = glob(os.path.join(directory,"*.cei"))
        with Pool(8) as p:
            iterator = p.imap(open_cei_preview, files)
            self.delete("1.0",END)
            self.tag_configure("centered", justify = "center")
            print(tags)
        
            for fname, preview, handle in iterator:
                try:
                    if not tags or not False in [i in handle for i in tags.split(" ")]:
                        win = WaterButton(self, preview, handle, fname, relief = FLAT, bd = 0, highlightthickness = 0)
                        self.window_create(END, window = win)
                        self.tag_add("centered","1.0",END)
                        self.update()
                except Exception as e:
                    print(e)

class WaterButton(Button_old):
    def __init__(self, master, source, tags, fname, **kwargs):
        super().__init__(master, **kwargs)
        self.master = master
        tk = ImageTk.PhotoImage(make_icon(source))
        wtk = ImageTk.PhotoImage(water(make_icon(source), tags))
        self.config(image = tk)
        self.tags = tags
        self.fname = fname
        self.config(command = self.opener)
        self.bind('<Enter>', lambda e: self.config(image = wtk))
        self.bind('<Leave>', lambda e: self.config(image = tk))

    def opener(self):
        fname = self.fname
        im, text = open_cei(fname)
        im = process(im)
        handle = get_handle(text)
        Viewer(self.master.master, im, handle, fname)

def make_icon(original):
    size = 1200//3
    if original.size[0]>original.size[1]:
        cropped = original.crop((original.size[0]//2-original.size[1]//2,0,original.size[0]//2+original.size[1]//2,original.size[1]))
        cropped = cropped.resize((size,size), Image.BICUBIC)
    else:
        original.thumbnail((size,original.size[1]))
        cropped = original.crop((0,0,size,size))
    return cropped

def amplify(x):
    scale = 1.1
    x = round(x**scale) if round(x**scale<256) else 255
    return x

def water(path, textdict = ";art;kemono"):
    main = Image.open(path) if isinstance(path, str) else path
    overlay = main.copy()
    overlay = overlay.filter(ImageFilter.GaussianBlur(radius=50))
    line = Image.new("L",(1,256),0)
    line.putdata(tuple([amplify(i) for i in range(256)]))
    black = Image.new("L",overlay.size,0)
    black.paste(line.resize((black.size[0],100), Image.BILINEAR),(0,black.size[1]-100))
    main.paste(overlay, (0,0), black)

    draw = ImageDraw.Draw(main)
    
    s = 16
    try:
        font = ImageFont.truetype("HelveticaNeueCyr-Medium.ttf", s)
    except Exception:
        font = ImageFont.truetype("calibri.ttf", s+2)
    n = 1
    text = get_handle(textdict)#f"{textlist[0]} by {textlist[1]}{' '*n}{textlist[2]}"
    
    while font.getsize(text)[0]>=main.size[0]:
        s-=1
        font = ImageFont.truetype("HelveticaNeueCyr-Medium.ttf", s)
    
    c = list(main.convert("L").crop((0,main.size[1]-20,main.size[0],main.size[1])).resize((1,1), Image.BILINEAR).getdata())[0]
    color = 255 if c<128 else 0
    draw.text((6, main.size[1]-32),text,color,font=font)
    return main

if __name__=="__main__":
    freeze_support()
    if not sys.argv[-1].endswith(".cei"):
        gui = GUI()
    else:
        fname = sys.argv[-1]
        #im, text = open_cei(fname)
        #im = process(im)
        #handle = get_handle(text)
        Viewer(None, None, None, fname)
