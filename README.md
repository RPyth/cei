# CEI - Color-Encoded Images
**CEI** is a raster graphics format with lossy compression similar to JPG intended for compact representation of artworks, photography and more. Software that has the capability of displaying CEI files can be referred to as a CEI Converter, or "ceicon", while many such programs were created previously, this is the first one intended for public use.
## Features
A short list of features directly related to the format itself:
- luminosity layer with 16x16 sector-based compression
- chromaticity layer with a 8-bit color palette & additional compression
- usual tags and "special" tags, such as image name and author
- multiple ways to decode each of the layers via interpolation selection

Some of the CEICON-related features:
- CEI decoder both via menu and drag & drop
- CEI generation dialogue with alternate compression settings
- directory preview mode for getting previews of sets of files
- tag search in directories, triggered via Ctrl+F, tags are separated with a single " "

## Requirements
- Python 3.10 and higher
- non-standard packages `Pillow`, `opencv-python`, `pyyaml` and `numpy`

## Creating a CEI

 1. Open the PNG/JPG to CEI dialogue via `File/Convert to CEI`
 2. Click on the question mark button next to the "Source" option, output filename will be generated automatically
 3. Enter a formula where numbers keep increasing and are between 0 and 1, recommended formula is x**0.5, but 0.5 can be changed to a higher value to trigger stronger compression
 4. Set the chroma slider to a non-zero value, 1 is smallest possible chroma size with lots of color bleeding, 100 is needlessly detailed, recommended value is 20
 5. Enter the tags and separate them with the " " character, to select a different separator, enter it as the first character instead (example: `;name:Exhibition Winners;author:photographer;Cat Exhibition;photo`)
 6. Click "Generate", then wait until the file appears, the application might freeze in process
 
 ## Additional File Information
As of version 0, CEI files have a structure similar to a Python dictionary with 7 keys: `version`, `size`, `csize`, `luminocity`, `chromaticity`, `palette`, `tags`, each with its own value represented by a bytes object created by the "separator" function, keys are stored as utf-8 characters. On top of lossy compression created by size reduction of each of luma sectors and chroma as a whole, chroma, luma and palette are compressed using `zlib`.
The first byte of the luma is always a so-called "infobyte", every sector has one preceding it, each infobyte contains size data in shape of `0xXXXZYYYW`, where the infobyte itself is a number from 0 to 255, X describes actual sector width minus one, Y describes actual sector height minus one and ZW tells whether or not the upscaled form of the sector is exactly 16 pixels in size. The rightmost and lowest sectors might deviate from the standard 16x16 sector size.
The entirety of the chromaticity layer is a reference to a color taken from the palette, totaling at 256 values, meaning that technically CEI is a 16-bit format, although the utilized color model - LSS, allows for three independent channels. Every value contains two chars.
LSS (Luminocity, Ratio 1, Ratio 2) is a color model where L is the average of each of the three R, G, B channels, and both S channels are a ratio of color one (C1) and color 2 (C2), as in $S = \dfrac{C_1*255}{C_1+C_2}$, colors one and two being R and G for S1 and G, B for S2.

> Written with [StackEdit](https://stackedit.io/).